import {
    defineStore
} from 'pinia'
import axios from "axios"
import SuperAgent from 'superagent'

export const useComicStore = defineStore("comic", {
    state: () => ({
        comic: [],
    }),
    getters: {
        getComic(state) {
            return state.comic
        }
    },
    actions: {
        async fetchComic(id = 1) {
            try {
                fetch(`https://cors-anywhere.herokuapp.com/http://xkcd.com/${id}/info.0.json`, {
                        method: 'GET',
                        headers: {
                            'Content-Type': 'application/json',
                            Accept: 'application/json',
                        }

                    })
                    .then(resp => resp.json())
                    .then(res => {
                        this.comic = res
                    })
                    .catch(error => console.error(`Error --> `, error))
                

            } catch (error) {
                console.log(error)
            }
        }
    }
})