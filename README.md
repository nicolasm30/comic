# Comic

Pagina Simple Desarrollada En VueJs v3, Vite y SASS

## Installation

Clonamos el proyecto en nuestro computador y lo abrimos con visual code 

Abrimos la terminal de Visual y ejecutamos lo siguientes comandos

```bash
npm i
```

Esperamos que se instalen todas las dependencias y seguido a esto ejecutamos
```bash
npm run dev
```

De esta manera nos correra nuestro Proyecto en un entorno local.

## Corregir problema de Cors

Para resolver el problema se utilizo cors-anywhere y para poder utilizarlo debemos dirigirnos a la url https://cors-anywhere.herokuapp.com/corsdemo en la pagina le damo click en el boton REQUEST TEMPORARY ACCESS TO DEMO SERVER. al primirlo esperamos un poco hasta que nos salfa el mensaje "You currently have temporary access to the demo server."

Con esto volvemos a nuestro proyecto en el navegador y refrescamos y el api queda funcional.

## License

[MIT](https://choosealicense.com/licenses/mit/)